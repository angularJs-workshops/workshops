/**
 * @author : pawel kaminski
 * @since : 11.08.2013 11:16
 *
 * description :
 */
(function(angular) {
    'use strict';
    var initialize = function(logger) {
        logger.info('app is initialized');
    };
    angular.
        module('workshops.join', ['ng', 'workshops.join.directive']).
        run(['$log', initialize]);
}(angular));