(function (angular) {
    'use strict';
    angular
        .module('workshops.timeline', ['ng', 'workshops.filter', 'restangular', 'workshops.timeline.directive'])
        .config(function ($locationProvider, RestangularProvider) {
            $locationProvider.html5Mode(true).hashPrefix('!');
            RestangularProvider.setBaseUrl('/api');
        }).run(function ($log) {
            $log.info('timeline app is initialized');
        });
}(angular));
