/**
 * @author : pawel kaminski
 * @since : 27.08.2013 20:10
 *
 * description :
 */
(function(angular) {
    'use strict';
    var controller = function($scope, modal, logger) {
        logger.log('Event Modal Controller initialized');
        $scope.close = function($event) {
            modal.hide();
        };
    };
    angular.
        module('workshops.timeline').
        controller('EventModalController', ['$scope', 'modal', '$log', controller]);
}(angular));