/**
 * @author : pawel kaminski
 * @since : 11.08.2013 11:18
 *
 * description :
 */
(function(angular) {
    'use strict';
    var Controller = function($scope) {
        $scope.credentials = {};
        $scope.loginPattern = /^[a-z][a-z0-9\.@\-_]{3,7}$/i;
        $scope.passwordPattern = /^[a-zA-Z0-9!#$%\^\.@\-_]{8,16}$/;
        // handlers
        $scope.handleSubmit = function(e) {
            var theForm = $scope.joinForm;
            return theForm.$valid && !theForm.$pristine;
        };
        // validate
        $scope.notSet = function(input) {
            return {hide: input.$pristine || !input.$error.required};
        };
        $scope.notValidatePattern = function(input) {
            return {hide: input.$pristine || !input.$error.pattern};
        };
        $scope.gotError = function(input) {
            return {error: !input.$pristine && input.$invalid};
        };
        $scope.validateSubmitButton = function(joinForm) {
            return joinForm.$invalid;
        };
    };
    angular.
        module('workshops.join').
        controller('JoinController', ['$scope', '$log', Controller]);
}(angular));