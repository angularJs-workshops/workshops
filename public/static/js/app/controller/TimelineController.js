(function (angular) {
    'use strict';
        var controller = function($scope, currentUserService, timelineService, userService, liveTimelineService) {
        var loggedUser = currentUserService.getLoggedUser();
        var now = function() {
            return new Date().getTime();
        };
        // simple controls locator
        var dialogs = {};
        function registerDialog(name, modal) {
            dialogs[name] = modal;
        }
        function getDialog(name) {
            return dialogs[name];
        }
        // event
        var resetEvent = function () {
            $scope.event.title = '';
            $scope.event.message = '';
            $scope.event.when = now();
            $scope.event.venue = '';
            $scope.event.invited = [];
        };
        $scope.event = {
            type: 'event',
            author: loggedUser,
            title: '',
            message: '',
            when: now(),
            venue: '',
            invited: []
        };
        $scope.createEvent = function() {
            resetEvent();
            var configuration = $scope.eventModal;
            getDialog(configuration.name).show(configuration);
        };
        $scope.saveEvent = function() {
            var newEvent = angular.copy($scope.event);
            newEvent.when = now();
            $scope.wallItems.push(newEvent);
            timelineService.create(newEvent);
            getDialog($scope.eventModal.name).hide();
        };
        $scope.isEventInvalid = function() {
            return false;
        };

        $scope.wallItems = timelineService.all();

        liveTimelineService.onNewItem(function(item) {
            $scope.wallItems.push(item);
        });

        $scope.message = {
            type: 'message',
            author: loggedUser,
            created_at: now(),
            message: ''
        };
        var resetMessage = function () {
            $scope.message.message = '';
        };

        $scope.post = function () {
            var newMessage = angular.copy($scope.message);
            newMessage.created_at = now();
            $scope.wallItems.push(newMessage);
            timelineService.create(newMessage);
            resetMessage();
        };
        $scope.users = userService.all();

        // configure event modal
        $scope.eventModal = {
            name: 'eventModal',
            templateUrl: '/timeline/eventModal',
            register: registerDialog,
            controller: 'EventModalController'
        };
    };
    angular
        .module('workshops.timeline')
        .controller('TimelineController', ['$scope', 'currentUserService', 'timelineService', 'userService', 'liveTimelineService', controller]);
})(angular);
