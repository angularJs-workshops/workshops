(function (angular) {
    'use strict';
    var controller = function($scope, userService) {
        $scope.users = userService.all();
        $scope.searchText = '';
    };
    angular
        .module('workshops.timeline')
        .controller('UsersController', ['$scope', 'userService', controller]);
})(angular);