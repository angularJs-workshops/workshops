/**
 * @author : pawel kaminski
 * @since : 11.08.2013 16:22
 *
 * description :
 */
(function(angular) {
    'use strict';
    angular.
        module('workshops.join.directive', ['directive.form']).
        run(['$log', function(logger) {logger.log('workshops.join.directive are initialized');}]);
}(angular));