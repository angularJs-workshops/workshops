/**
 * @author : pawel kaminski
 * @since : 29.08.2013 20:38
 *
 * description :
 */
(function(angular) {
    'use strict';
    angular.
        module('workshops.timeline.directive', ['directive.form', 'directive.dialog']).
        run(['$log', function(logger) {logger.log('workshops.timeline.directive are initialized');}]);
}(angular));