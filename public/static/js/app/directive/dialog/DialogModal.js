/**
 * @author : pawel kaminski
 * @since : 26.08.2013 22:51
 *
 * description :
 */
(function(angular) {
    'use strict';
    var ModalDirective = function($http, $templateCache, $controller, $compile) {
        var link = function(scope, elem) {
            var modalController, contentScope;
            var overlay = angular.element(elem[0].querySelector('.modal-backdrop'));
            var modalElem = angular.element(elem[0].querySelector('.modal'));
            var contentElement = angular.element(elem[0].querySelector('.modal-body'));

            function show() {
                elem.show();
                overlay.addClass('in');
                modalElem.addClass('in');
            }

            function hide() {
                elem.hide();
                overlay.removeClass('in');
                modalElem.removeClass('in');
                clearContent();
            }

            function clearContent() {
                if (contentElement) {
                    contentElement.html('');
                }
                if (modalController) {
                    modalController = null;
                }
                if (contentScope) {
                    contentScope.$destroy();
                    contentScope = null;
                }
            }

            function update(result) {
                var template = result.template;
                var configuration = result.configuration;

                clearContent();
                contentElement.html(template);

                // create sibling
                contentScope = scope.$parent.$new();
                contentScope.data = configuration.data;

                var linking = $compile(contentElement.contents());
                linking(contentScope);

                if (configuration.controller) {
                    modalController = $controller(configuration.controller, {"$scope": contentScope, "modal": modal});
                }
            }

            function loadContent(configuration, cache) {
                $http.
                    get(configuration.templateUrl, {cache: cache}).
                    then(function(response) {
                             return {template: response.data, configuration: configuration};
                         }).
                    then(update,
                         function() {
                             clearContent();
                         });
            }

            function showModal(configuration) {
                loadContent(configuration, $templateCache);
                show();
            }
            function hideModal() {
                hide();
            }
            var modal = {
                show: showModal,
                hide: hideModal
            };

            scope.$watch('configuration', function(configuration) {
                if (angular.isObject(configuration)) {
                    if (angular.isFunction(configuration.register)) {
                        configuration.register(configuration.name, modal);
                    }
                }
            });
            scope.close = function($event) {
                hide();
                $event.preventDefault();
            };
            hide();
        };
        return {
            template: '<div>' +
                          '<div class="modal-backdrop"></div>' +
                          '<div class="modal fade">' +
                            '<div class="modal-header">' +
                                '<button class="close" type="button" data-dismiss="modal" aria-hidden="true" ' +
                                    'data-ng-click="close($event)">&nbsp;&times;</button>' +
                                '<h3 data-ng-transclude=""></h3>' +
                            '</div>' +
                            '<div class="modal-body"></div>' +
                          '</div>' +
                      '</div>',
            transclude: true,
            replace: true,
            restrict: 'A',
            scope: {configuration: '=wsDgModal'},
            link: link
        };
    };
    angular.
        module('directive.dialog').
        directive('wsDgModal', ['$http', '$templateCache', '$controller', '$compile', ModalDirective]);
}(angular));