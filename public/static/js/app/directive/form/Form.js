/**
 * @author : pawel kaminski
 * @since : 11.08.2013 16:24
 *
 * description :
 */
(function(angular) {
    'use strict';
    angular.
        module('directive.form', ['ng']).
        run(['$log', function(logger) {logger.log('form directives are initialized');}]);
}(angular));