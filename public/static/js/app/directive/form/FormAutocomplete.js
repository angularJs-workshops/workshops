/**
 * @author : pawel kaminski
 * @since : 11.08.2013 16:27
 *
 * description :
 */
(function(angular) {
    'use strict';
    var Autocomplete = function(logger) {
        var $ = angular.element;
        var TAB = $.ui.keyCode.TAB;
        var items = [];

        function split(val) {
            return val.split(/,\s*/);
        }

        function extractLast(term) {
            return split(term).pop();
        }

        var link = function(scope, elem, attr, ngModel) {
            function applyValue(selectedValue) {
                scope.$apply(function() {
                    ngModel.$modelValue.push(selectedValue);
                    ngModel.$setViewValue(ngModel.$modelValue.slice(0));
                    ngModel.$viewValue = formatter(ngModel.$modelValue);
                    ngModel.$render();
                });
            }
            function formatter(modelValue) {
                var result = '';
                if (angular.isArray(modelValue) && modelValue.length > 0) {
                    angular.forEach(modelValue, function(user) {
                        result += user.login + ', ';
                    });
                }
                return result;
            }

            var autocompleteConfig = {
                minLength: 0,
                source: function(request, response) {
                    // delegate back to autocomplete, but extract the last term
                    var token = $.ui.autocomplete.escapeRegex(extractLast(request.term));
                    var filtered = [];
                    var pattern = new RegExp('^.*' + token + '.*$', 'i');
                    angular.forEach(items, function(user) {
                        if (user.login.match(pattern)) {
                            filtered.push({
                                              label: user.login,
                                              value: user
                                          });
                        }
                    });
                    logger.log('source : ', angular.toJson(filtered));
                    response(filtered);
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                select: function(event, ui) {
                    applyValue(ui.item.value);
                    return false;
                }
            };
            var handleKeyDown = function(event) {
                if (event.keyCode === TAB && $(this).data("ui-autocomplete").menu.active) {
                    event.preventDefault();
                }
            };
            // configure control
            elem.
                bind("keydown", handleKeyDown).
                autocomplete(autocompleteConfig);

            scope.$watch(attr.wsFmAutocomplete, function(current) {
                items = current && angular.isArray(current) ? current : [];
            }, false);

            ngModel.$render = function() {
                elem.val(ngModel.$viewValue || []);
            };
            ngModel.$parsers.unshift(function(value) {
                if (angular.isObject(value)) {
                    return value;
                }
                return ngModel.$modelValue;
            });
            ngModel.$formatters.unshift(formatter);
        };
        return  {
            restrict: 'A',
            require: 'ngModel',
            link: link
        };
    };
    angular.
        module('directive.form').
        directive('wsFmAutocomplete', ['$log', Autocomplete]);
}(angular));