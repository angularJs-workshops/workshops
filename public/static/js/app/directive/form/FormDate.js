/**
 * @author : pawel kaminski
 * @since : 11.08.2013 16:27
 *
 * description :
 */
(function(angular) {
    'use strict';
    var FormDate = function(logger) {
        var link = function(scope, elem, attr, ngModel) {
            logger.log('date control initialized');
            var format = attr.wsFmDate || 'yyyy-MM-dd';
            var formatter = function(modelValue) {
                var date = new Date(modelValue);
                var month = date.getMonth() + 1;

                return date.getFullYear() + '-' + (month < 10 ? '0' + month : month) + '-' + date.getDate();
            };
            ngModel.$render = function() {
                var viewValue = ngModel.$viewValue || formatter(new Date().getTime());
                elem.val(viewValue);
            };
            ngModel.$parsers.unshift(function(viewValue) {
                var miliseconds = Date.parse(viewValue);
                if (miliseconds) {
                    ngModel.$setValidity('date', true);
                    return miliseconds;
                } else {
                    ngModel.$setValidity('date', false);
                    return null;
                }
            });
            ngModel.$formatters.push(formatter);
        };
        return {
            restrict: 'A',
            require: 'ngModel',
            link: link
        }
    };
    angular.
        module('directive.form').
        directive('wsFmDate', ['$log', FormDate]);
}(angular));