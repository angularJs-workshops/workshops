/**
 * @author : pawel kaminski
 * @since : 11.08.2013 16:27
 *
 * description :
 */
(function(angular) {
    'use strict';
    var DatePicker = function(logger) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function(scope, elem, attr, ngModel) {
                logger.log('date picker initialized');
                function dateChanged(dateString) {
                    logger.log('date selected : ', dateString);
                    scope.$apply(function() {
                        ngModel.$setViewValue(dateString);
                    });
                }
                var format = attr.wsFmDatepicker || 'yy-mm-dd';
                elem.datepicker({
                                    "dateFormat": format,
                                    "onSelect": dateChanged
                                });

                ngModel.$render = function() {
                    elem.val(ngModel.$viewValue || new Date().getTime());
                };
                ngModel.$parsers.unshift(function(viewValue) {
                    ngModel.$setValidity('date', true);
                    return new Date(viewValue).getTime();
                });
                ngModel.$formatters.push(function(modelValue) {
                    var date = new Date(modelValue);
                    return angular.element.datepicker.formatDate(format, date);
                });
            }
        }
    };
    angular.
        module('directive.form').
        directive('wsFmDatepicker', ['$log', DatePicker]);
}(angular));