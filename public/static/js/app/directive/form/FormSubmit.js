/**
 * @author : pawel kaminski
 * @since : 11.08.2013 16:27
 *
 * description :
 */
(function(angular) {
    'use strict';
    var FormSubmit = function($parse) {
        return function(scope, elem, attr) {
            var callback = $parse(attr.wsFmSubmit);
            elem.on('submit', function(e) {
                scope.$apply(function() {
                    var result = callback(scope, {event: e});
                    if (!result) {
                        e.preventDefault();
                    }
                });
            });
        };
    };
    angular.
        module('directive.form').
        directive('wsFmSubmit', ['$parse', FormSubmit]);
}(angular));