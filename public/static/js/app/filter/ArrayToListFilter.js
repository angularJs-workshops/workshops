/**
 * @author : pawel kaminski
 * @since : 28.08.2013 20:44
 *
 * description :
 */
(function(angular) {
    'use strict';
    var ArrayToListFilter = function() {
        return function(model) {
            if (angular.isArray(model)) {
                return model.join(', ');
            } else {
                return model;
            }
        }
    };
    angular.
        module('workshops.filter', ['ng']).
        filter('arrayToList', ArrayToListFilter);
}(angular));