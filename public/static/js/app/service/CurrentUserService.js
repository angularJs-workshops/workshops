(function (angular) {
    'use strict';
    var Service = function($log, $location) {
        this.getLoggedUser = function() {
            return $location.search().login;
        };
    };
    angular
        .module('workshops.timeline')
        .service('currentUserService', ['$log', '$location', Service]);
})(angular);