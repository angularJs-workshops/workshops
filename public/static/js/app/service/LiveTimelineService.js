(function(angular) {
    'use strict';
    var Service = function($rootScope) {
        var client = new Faye.Client('http://workshop.invaders.co:8000/faye', { timeout: 120 });

        this.onNewItem = function(callback) {
          client.subscribe('/wall_items', function(item) {
            $rootScope.$apply(function() {
              callback(item);
            });
          });
        };
    };
    Service.$inject = ['$rootScope'];
    angular.module('workshops.timeline').service('liveTimelineService', Service);
}(angular));
