(function(angular) {
    'use strict';
    var Service = function(Restangular) {
        var base = Restangular.all('wall_items');

        this.all = function() { return base.getList(); };
        this.create = function(item) { return base.post(item); };
    };
    Service.$inject = ['Restangular'];
    angular.
        module('workshops.timeline').
        service('timelineService', Service);
}(angular));
