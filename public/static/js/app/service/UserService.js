(function(angular) {
    'use strict';
    var Service = function(Restangular) {
        var base = Restangular.all('users');

        this.all = function() { return base.getList(); };
    };
    Service.$inject = ['Restangular'];
    angular.
        module('workshops.timeline').
        service('userService', Service);
}(angular));
