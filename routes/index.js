var http = require('http');
var post = function(login, pass, callback) {
    var now = new Date().getTime();
    var newUserDate = JSON.stringify({
                                                "login": login,
                                                "password": pass,
                                                "last_login": now,
                                                "created_at": now,
                                                "active": true
                                            });
    var post_options = {
        hostname: 'workshop.invaders.co',
        port: '3000',
        path: '/api/users',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': newUserDate.length
        }
    };
    var request = http.request(post_options, function(response) {
        console.log('STATUS: ' + response.statusCode);
        console.log('HEADERS: ' + JSON.stringify(response.headers));
        response.setEncoding('utf8');
        response.on('data', function(chunk) {
            console.log('data ' + chunk);
            callback();
        });
    });
    request.on('error', function(e) {
        console.log('problem with request: ' + e.message);
    });

    // post the data
    request.write(newUserDate);
    request.end();
};
/**
 * renders main page
 *
 * @param req
 * @param res
 */
exports.index = function(req, res) {
    res.render('join', { title: 'Workshops Yeah!!' });
};

/**
 * authenticates request and redirects to app
 * @param req
 * @param res
 */
exports.auth = function(req, res) {
    // call api to authenticate
    var login = req.body.login;
    post(login, req.body.password, function() {
        console.log('returned from post');
        // redirect inside app
        res.redirect('/timeline?login=' + login);
    });
};
