/**
 * Module dependencies.
 */

var express = require('express')
    , routes = require('./routes')
    , timelinePage = require('./routes/timeline')
    , http = require('http')
    , httpProxy = require('http-proxy')
    , path = require('path');

var app = express();

// ----------- CONFIG -----------
// all environments
app.locals.pretty = true;
app.set('port', 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}


var options = {
//    pathnameOnly: true, // not assuming localhost - might be VM
    router: {
        'localhost/api': 'workshop.invaders.co:3000/api',
        'localhost': 'localhost:' + app.get('port')
    }
};
httpProxy.createServer(options).listen(8080);


// ----------- METHODS ----------
// GET
app.get('/', routes.index);
app.get('/timeline', timelinePage.main);
app.get('/timeline/eventModal', timelinePage.eventModal);

// POST
app.post('/auth', routes.auth);

//START
http.createServer(app).listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});
