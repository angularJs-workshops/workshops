'use strict';
describe("TimelineController", function() {
    var scope, itemsPromise, timelineServiceFake, userServiceFake, currentUserServiceFake;
    beforeEach(module('workshops.timeline'));
    beforeEach(inject(function($controller) {
        scope = {};
        itemsPromise = { then: function(fn) { fn([{id: 1, type: 'message'}]) }, push: angular.noop };
        timelineServiceFake = { all: function() { return itemsPromise; }, create: jasmine.createSpy() };
        userServiceFake = { create: angular.noop, all: angular.noop };
        currentUserServiceFake = { getLoggedUser: function() { return 'alice'; } };

        $controller('TimelineController', { '$scope': scope, 'timelineService': timelineServiceFake,
          'currentUserService': currentUserServiceFake, 'userService': userServiceFake })
    }));

    it("gets the items list from timelineService", function() {
        expect(scope.wallItems).toEqual(itemsPromise);
    });

    it("posts new messsages to the backend", function() {
        scope.post();

        expect(timelineServiceFake.create).toHaveBeenCalledWith({type: 'message', author: 'alice',
          message: '', created_at: jasmine.any(Number)});
    });
});
