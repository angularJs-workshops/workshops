'use strict';
describe("UsersController", function() {
    beforeEach(module('workshops.timeline'))

    it("gets the items list from userService", inject(function($controller) {
        var scope = {};
        var itemsPromise = { then: function() {} };
        var userServiceFake  = { all: function() { return itemsPromise; } };

        $controller('UsersController', { '$scope': scope, 'userService': userServiceFake })

        expect(scope.users).toEqual(itemsPromise)
    }));
});
