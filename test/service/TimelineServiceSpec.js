'use strict';

describe("Timeline serivce", function() {
    beforeEach(module('workshops.timeline'));

    it("gets a list of timeline items", inject(function($httpBackend, timelineService) {
        $httpBackend.whenGET('/api/wall_items').respond(201, [{id: 1, name: 'workshop'}]);

        var wallItems;
        timelineService.all().then(function(items) { wallItems = items; });
        $httpBackend.flush();

        expect(wallItems.length).toEqual(1);
        expect(wallItems[0].id).toEqual(1);
        expect(wallItems[0].name).toEqual('workshop');
    }));

    it("posts messages to timeline", inject(function($httpBackend, timelineService) {
        $httpBackend.expectPOST('/api/wall_items', {name: 'circus with no animals'}).respond(201, '')

        timelineService.create({name: 'circus with no animals'});

        $httpBackend.flush();
    }));
});
