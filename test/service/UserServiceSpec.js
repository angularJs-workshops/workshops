'use strict';
describe("UserService", function() {
    beforeEach(module('workshops.timeline'))

    it("gets the list of user from the backend", inject(function($httpBackend, userService) {
        $httpBackend.whenGET('/api/users').respond(200, [{id: 3, name: 'Alice'}]);

        var resolvedUsers;
        userService.all().then(function(users) { resolvedUsers = users; });
        $httpBackend.flush();

        expect(resolvedUsers.length).toEqual(1);
        expect(resolvedUsers[0].name).toEqual('Alice');
        expect(resolvedUsers[0].id).toEqual(3);
    }));
});
